import React from 'react'
import {useNavigate} from 'react-router-dom'

function OrderSummery() {
  const navigate = useNavigate()
  const handleGoBack = () => {
    navigate(-1)
  }
  return (
    <div>
       <div className='orderconfirm-message'>
          <h3>order confirmed</h3><br />
       </div>
       <div>
          <button type="button" onClick={handleGoBack}>Go Back</button>
       </div>
    </div>
  )
}

export default OrderSummery;