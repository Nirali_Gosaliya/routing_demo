import React from 'react'
import {useNavigate} from 'react-router-dom'

function Home() {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('order-summery',{replace: true})
  }
  return (
   <div>
         <div>Home page</div><br />
         <button type="button" onClick={handleClick}>Place Order</button>
   </div>
   
  )
}

export default Home;