
import './App.css';
import {Routes,Route} from 'react-router-dom'
import Home from './Components/Home';
import About from './Components/About';
import NavBar from './Components/NavBar';
import OrderSummery from './Components/OrderSummery';
import NoMatchFound from './Components/NoMatchFound';
import Products from './Components/Products';
import FeaturedProducts from './Components/FeaturedProducts';
import NewProducts from './Components/NewProducts';
import Users from './Components/Users';
import UserDetails from './Components/UserDetails';
import Admin from './Components/Admin';
import Profile from './Components/Profile';



function App() {
  return (
    <>
      <NavBar />
       <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='about' element={<About />}></Route>
        <Route path='order-summery' element={<OrderSummery />}></Route>
        <Route path='products' element={<Products />}>
        {/* if we use index instead of path props  the child component is rendered using parent component */}
        {/* It takes the path of parent component */}
            <Route index element={<FeaturedProducts />}></Route>
            <Route path='featured' element={<FeaturedProducts />}></Route>
            <Route path='newproducts' element={<NewProducts />}></Route>
        </Route>
        <Route path='*' element={<NoMatchFound />}></Route>
        <Route path="users" element={<Users />}>
            <Route path=":userId" element={<UserDetails />}></Route>
            <Route path="admin" element={<Admin />}></Route>
        </Route>
       <Route path='profile' element={<Profile />}></Route>
    </Routes>
    </>
   
  );
}

export default App;
