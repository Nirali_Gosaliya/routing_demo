import React from "react";
import {Link,Outlet} from 'react-router-dom'

function Products() {
  return (
    <>
      <div>
        <input type="search" placeholder="Search"></input><br /><br />
      </div>

      <div>
          <nav className="nested_routes">
              <Link to='featured' className="featured_new_link">Featured</Link>
              <Link to='newproducts' className="featured_new_link">new</Link>
          </nav>
         <Outlet />
      </div>
    </>
  );
}

export default Products;
