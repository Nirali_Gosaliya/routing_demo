import React from 'react'
import {Outlet,useSearchParams} from 'react-router-dom'

function Users() {
  const [searchParams,setSearchParams] = useSearchParams();
  const activeusers = searchParams.get('filter') === 'active'
  return (
  <>
    <div>
    <h2>User 1</h2>
        <h2>User 2</h2>
        <h2>User 3</h2>
        <Outlet />
    </div>

    <div>
        <button onClick={()=>setSearchParams({filter:'active'})}>Active users</button>
        <button onClick={()=>setSearchParams({})}>Reset filter2</button>
    </div>
    {
      activeusers ? <h2>Show active users</h2> : <h2>Show all users</h2>
    }
  </>
  )
}

export default Users;