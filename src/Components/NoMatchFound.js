import React from 'react'

function NoMatchFound() {
  return (
    <div>
        <div>
            Page not found
        </div>
    </div>
  )
}

export default NoMatchFound;