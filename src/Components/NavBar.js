import {NavLink} from 'react-router-dom';
import '../assets/CSS/navbar.css'

function Navbar () {
    return(
        <nav>
            <NavLink to='/'>Home</NavLink>
            <NavLink to='/about'>About</NavLink>
            <NavLink to='/products'>Products</NavLink>
            <NavLink to='/profile'>Profile</NavLink>
        </nav>
    )
}

export default Navbar;